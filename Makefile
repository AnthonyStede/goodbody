CCPP:=g++

CXXFLAGS = $(shell pkg-config --cflags cairo gtk+-3.0 librsvg-2.0 tinyxml2 libcbor)
LDFLAGS = $(shell pkg-config --libs cairo gtk+-3.0 librsvg-2.0 tinyxml2 libcbor)

SRC := $(wildcard src/*.cpp)
OBJS := $(SRC:.cpp=.o)

.PHONY : all clean
all : main
main : $(OBJS)
	$(CCPP) $(CXXFLAGS) $(OBJS) -o $@ $(LDFLAGS)
%.o : %.cpp
	$(CCPP) $(CXXFLAGS) -o $@ -c $< $(LDFLAGS) -I.
clean:
	rm -f src/*.o *~ main
