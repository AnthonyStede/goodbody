// Le fichier Client.h créé les chaînes d'informations qui seront ensuite
// envoyées en UDP au format cbor sur le port 6789. Par ailleurs, ce fichier gère
// aussi les déplacements de tous les élements du fichier svg.

#ifndef CLIENT_H
#define CLIENT_H

// --includes--
#include <cbor.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <netdb.h>
#include <string.h>
#include <thread>
#include <iostream>
#include <chrono>

// --packages--
using namespace std;

// --déclarations--
class Client{

  // --variables--
private:
  bool lancer = false;
  unsigned char *buffer;
  char *ip = "127.0.0.1";
  struct sockaddr_in sin;
  string rotationTerre = "360";
  string rotationTerreEntiere = "|rotate("+rotationTerre+" 379 439)|";
  int decompteTankJoueur = 0;
  int tankJoueurActif = 1;
  string positionTank1X = "|120|";
  string positionTank1Y = "|98|";
  string positionTank2X = "|10000|";
  string positionTank2Y = "|10000|";
  string positionRoquetteXFormat = "120";
  string positionRoquetteYFormat = "98";
  string positionRoquetteX = "|"+positionRoquetteXFormat+"|";
  string positionRoquetteY = "|"+positionRoquetteYFormat+"|";
  string positionArmementX = "|120|";
  string positionArmementY = "|98|";
  string positionObusXFormat = "20000";
  string positionObusYFormat = "20000";
  string positionObusX = "|"+positionObusXFormat+"|";
  string positionObusY = "|"+positionObusYFormat+"|";
  string positionPretObusX = "|-210|";
  string positionPretObusY = "|-285|";
  string positionRechargementObusX = "|30000|";
  string positionRechargementObusY = "|30000|";
  string positionPretRoquetteX = "|-210|";
  string positionPretRoquetteY = "|-307|";
  string positionRechargementRoquetteX = "|30000|";
  string positionRechargementRoquetteY = "|30000|";
  string positionEnnemiXFormat = "50000";
  string positionEnnemiYFormat = "50000";
  string positionEnnemiX = "|"+positionEnnemiXFormat+"|";
  string positionEnnemiY = "|"+positionEnnemiYFormat+"|";
  int rechargementObus = 0;
  int rechargementRoquette = 0;
  bool tirObusEnCours = false;
  bool tirRoquetteEnCours = false;
  int countdownEnnemi = 30;
  int typeOfEnnemi;
  bool ennemiEnDeplacement = false;
  int nombreDeplacementEnnemi = 0;

  // --fonctions--
public:
  Client();
  void bouclePrincipale();
  void deplacementTerre();
  void deplacementTankJoueur();
  void tirerObus();
  void tirerRoquette();
  void deplacementObus();
  void deplacementRoquette();
  void lancement();
  int creerEnnemi();
  void deplacementEnnemi(int typeEnnemi);
};
#endif
